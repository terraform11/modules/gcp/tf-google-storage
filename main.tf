
resource "google_storage_bucket" "primary" {
  name          = "${var.name}"
  location      = "${var.location}"
  project       = "${var.project}"
  storage_class = "${var.storage_class}"

  lifecycle_rule {
      action {
          type = "${var.type}"
          storage_class = "${var.action_storage_class}"
      }
      condition {
          with_state = "${var.with_state}"
      }
  }
  
}