output "name" {
  value       = join("", google_storage_bucket.default.*.name)
  description = "The name of bucket"
}