variable "project" {
  description = "The ID of the project in which the resource belongs."
}

variable "name" {
  description = "The cluster name."
}

variable "location" {
  description = "The location (region or zone) in which the cluster master will be created."
}

variable "storage_class" {
  description = "The minimum version of the master."
}

variable "action_storage_class" {
  description = "The minimum version of the master."
}

variable "type" {
  description = "The minimum version of the master."
}

variable "with_state" {
  description = "The minimum version of the master."
}




